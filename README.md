# **Under Construction** #

This is a fork of GotoLink's paraknight (github)
[https://github.com/GotoLink/paraknight](https://github.com/GotoLink/paraknight)

TV Mod is fixed and extended with FFmpeg support. No xuggler anymore!

1. This is experimental and may unstable
2. Doesn't support multiplayer (officially) (multiplayer may theoretically work if all client have the same videos in their folder)

**RECIPES**

TV
![2016-07-30_14.58.56.png](https://bitbucket.org/repo/aLa8qb/images/3040193939-2016-07-30_14.58.56.png)
TV REMOTE
![2016-07-30_15.02.03.png](https://bitbucket.org/repo/aLa8qb/images/2076735197-2016-07-30_15.02.03.png)
![    ](https://bitbucket.org/qwertz19281/beama/downloads/paraknight.png)
