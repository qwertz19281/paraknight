package ffmpeg_pkg;

// 6.10.2018

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class PkgWorker {
	static MessageDigest shaDigest;

	/*public static void main(String[] args) throws Exception {
		shaDigest=MessageDigest.getInstance("SHA-256");
		System.out.println("369d027a1f6843c14cff6d7af85367c3d437c5597c614516102df12903d1d2b1");
		System.out.println(new String(hex(digestFile(new File("./ffmpeg.exe")))));
		System.out.println(new String(hex(new byte[] {(byte)0x12,(byte)0x56,(byte)0x90,(byte)0xCD,(byte)0xFE,(byte)0xEF})));
	}*/

	public static void extract(File dir) throws NoSuchAlgorithmException, IOException {
		shaDigest=MessageDigest.getInstance("SHA-256");
		String ops=System.getProperty("os.name").toLowerCase();
		String oscode=null,oscode2=null;
		boolean arch=System.getProperty("os.arch").contains("64");
		if(ops.contains("win")){
			System.out.println("OS: Windows");os=1;oscode=(arch?"w64/ffmpeg.exe":"w32/ffmpeg.exe");oscode2=(arch?"w64/ffmpeg":"w32/ffmpeg");
		} else if(ops.contains("mac")){
			System.out.println("OS: Mac");os=2;oscode="m64/ffmpeg";oscode2=oscode;
		} else if(ops.contains("nux")||ops.contains("nix")||ops.contains("aix")){
			System.out.println("OS: Unix");os=0;oscode=(arch?"l64/ffmpeg":"l32/ffmpeg");oscode2=oscode;
		}
		File of=new File(dir,os==1?"ffmpeg.exe":"ffmpeg");
		//System.out.println("[FFUpdater] To "+of);
		//File of2=new File(of.getAbsolutePath()+".part");
		String sha="none";
		if(of.exists()) {
			//synchronized(instance.waitnot) {instance.waited=true;instance.waitnot.notify();}
			sha=new String(hex(digestFile(of)));
		} else {
			//synchronized(instance.waitnot) {instance.waited=true;instance.wait=true;instance.waitnot.notify();}
		}
		System.out.println("[FFUpdater] Local Hash: "+sha);
		HttpURLConnection uc=(HttpURLConnection)new URL("https://bitbucket.org/qwertz19281/ffupdater-dist/raw/master/dist/checksums.sha256").openConnection();
		uc.setConnectTimeout(10000);
		uc.setReadTimeout(10000);
		uc.setUseCaches(false);
		BufferedReader br=new BufferedReader(new InputStreamReader(uc.getInputStream()));
		String lin;
		while((lin=br.readLine())!=null) {
			//System.out.println(lin);
			if(lin.endsWith(oscode)||lin.endsWith(oscode.replace('/', '\\'))) {
				System.out.print("[FFUpdater] Online Hash: "+lin);
				if(lin.startsWith(sha)&&of.exists()) {
					System.out.println(" EQUAL");
					br.close();
					markExec(of);
					return;
				}else {
					System.out.println(" DIFF");
					System.out.println("[FFUpdater] Updating");
					try {
						File ofbak=new File(of.getPath()+".bak");
						Files.copy(of.toPath(), ofbak.toPath(), StandardCopyOption.REPLACE_EXISTING);
					}catch(Exception ex) {
						ex.printStackTrace(System.out);
					}

					if(!unzipdl(new URL("https://bitbucket.org/qwertz19281/ffupdater-dist/raw/master/dist/"+oscode2+".zip"),dir)) {
						/*System.out.println("[FFUpdater] UnZIP dl failed",true);
						InputStream is=openStream(new URL("https://bitbucket.org/qwertz19281/ffupdater-dist/raw/master/dist/"+oscode));
						FileOutputStream os=new FileOutputStream(of2);
						byte[] buf=new byte[4096];
						int len;
						while((len=is.read(buf))!=-1) {
							os.write(buf, 0, len);
						}
						os.close();
						is.close();*/
						br.close();
						throw new IOException("Download Failed");
					}
					System.out.println("[FFUpdater] Done");
					uc.disconnect();
					br.close();
					return;
				}
			}
		}
		System.out.println("[FFUpdater] No occurence of "+oscode);
		uc.disconnect();
	}

	public static void finDL(File of,File of2) throws IOException {
		//synchronized(instance.dlLock) {
			if(of.exists()&&!of.delete()) {
				throw new IOException("[FFUpdater] Cannot delete ffmpeg.exe, Please close all instances or delete manually");
			}
			of2.renameTo(of);
			/*if(os==0) {
				Files.setPosixFilePermissions(of.toPath(), PosixFilePermissions.fromString("rwxrwxr-x"));
			}*/
			markExec(of);
		//}
	}
	
	public static void markExec(File file) {
		if(!file.canExecute()) {
			file.setExecutable(true);
		}
		if(!file.canExecute()){
			System.err.println("WARNING: Setting FFmpeg to executable failed");
		}
	}

	public static boolean unzipdl(URL iu,File out) {try {
		InputStream is=openStream(iu);
		ZipInputStream zis = new ZipInputStream(new BufferedInputStream(is,16384));
		ZipEntry zipEntry = zis.getNextEntry();
		byte[] buffer=new byte[4096];
		while(zipEntry != null){
			File outFile = new File(out,zipEntry.getName());
			File newFile=File.createTempFile(zipEntry.getName(), ".part", out);
			FileOutputStream fos = new FileOutputStream(newFile);
			int len;
			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			fos.close();
			finDL(outFile,newFile);
			zipEntry = zis.getNextEntry();
		}
		zis.closeEntry();
		zis.close();
		return true;
	}catch(IOException ex) {ex.printStackTrace();return false;}}

	public static InputStream openStream(URL ul) throws IOException {
		System.out.println("[FFUpdater] Start DL: "+ul.toString());
		URLConnection uc=ul.openConnection();
		uc.setConnectTimeout(10000);
		uc.setReadTimeout(10000);
		uc.setUseCaches(false);
		String redir=uc.getHeaderField("Location");
		while(redir!=null){
			uc=(HttpURLConnection)new URL(redir).openConnection();
			uc.setConnectTimeout(10000);
			uc.setReadTimeout(10000);
			uc.setUseCaches(false);
			redir=uc.getHeaderField("Location");
		}
		return uc.getInputStream();
	}

	public static int os=-1; //0=Unix 1=Windows 2=Mac

	public static byte[] digestFile(File f) throws IOException {
		FileInputStream fis=new FileInputStream(f);
		byte[] barr=new byte[4096];
		int c=0;
		shaDigest.reset();
		while ((c=fis.read(barr))!=-1) {
			shaDigest.update(barr,0,c);
		}
		fis.close();
		return shaDigest.digest();
	}

	static char[] hexal="0123456789abcdef".toCharArray();

	public static char[] hex(byte[] in) {
		char[] o=new char[in.length*2];
		for(int i=0;i<o.length;i+=2) {
			byte b=in[i/2];
			o[i]=hexal[(b>>4)&0x0f];
			o[i+1]=hexal[b&0x0f];
		}
		return o;
	}
}
