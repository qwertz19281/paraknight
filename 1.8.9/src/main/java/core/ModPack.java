package core;

import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.world.WorldEvent;
import tvmod.EntityTV;
import tvmod.ItemTV;
import tvmod.ItemTVRemote;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import org.lwjgl.opengl.GL11;

@Mod(modid = "paraknight", name = "Paraknight Mod Pack", useMetadata = true, updateJSON = "https://bitbucket.org/qwertz19281/paraknight/raw/FFMPEG/update_info.json")
public final class ModPack {
    public final static String FOLDER = "paraknight:";
    @Instance("paraknight")
    public static ModPack instance;
    @SidedProxy(clientSide = "core.ClientProxy", serverSide = "core.CommonProxy")
    public static CommonProxy proxy;
    public static Item ride, wrench, bikePart;
    public static boolean shuffle = false, xuggLoaded = true, linscale=false;
    public static int width, height, soundRange,vw,vh,vfps,asr,ac,preWaitDelay,audPreFeed;
    public static Item tv, tvRemote;

    @EventHandler
    public void load(FMLInitializationEvent event) {
        proxy.registerHandlers();
        NetworkRegistry.INSTANCE.registerGuiHandler(this, proxy);
        MinecraftForge.EVENT_BUS.register(this);

        if(event.getSide()==Side.CLIENT){
            registerRender(tv);
            registerRender(tvRemote);
        }

        /*while(ffloaded){
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }*/
    }

    public static Thread t;
    
    @EventHandler
    public void preLoad(FMLPreInitializationEvent event) {
        t=new Thread(){
            @Override
            public void run() {
                try {
                    ffmpeg_pkg.PkgWorker.extract(new File("."));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        t.setDaemon(true);
        t.setName("FFmpeg updater");
        t.setPriority(4);
        t.start();
        Configuration config = new Configuration(event.getSuggestedConfigurationFile());
        boolean enableBikes = config.get("General", "EnableSteamBikes", true).getBoolean(true);
        boolean enableLawnMower = config.get("General", "EnableLawnMawer", true).getBoolean(true);
        boolean enableTVMod = config.get("General", "EnableTVMod", true).getBoolean(true);
        /*if (enableBikes || enableLawnMower) {
            ride = new ItemSpawner().setUnlocalizedName("paraknight:").setTextureName("paraknight:");
            wrench = new Item().setMaxStackSize(1).setMaxDamage(100).setUnlocalizedName("paraknight:wrench").setCreativeTab(CreativeTabs.tabTransport).setTextureName("paraknight:wrench");
            GameRegistry.registerItem(ride, "RideSpawner");
            GameRegistry.registerItem(wrench, "Wrench");
            GameRegistry.addShapelessRecipe(new ItemStack(wrench), new ItemStack(Items.iron_ingot), new ItemStack(Items.coal));
        }
        if (enableBikes) {
            bikePart = new ItemBikePart().setUnlocalizedName("steambikes:").setTextureName("paraknight:");
            GameRegistry.registerItem(bikePart, "BikePart");
            GameRegistry.addRecipe(new ItemStack(bikePart, 1, 0), " I ", "IGI", " I ", 'I', Items.iron_ingot, 'G', Items.gold_ingot);
            GameRegistry.addRecipe(new ItemStack(bikePart, 1, 1), "CSR", "III", "WGW", 'C', Blocks.chest, 'S', Items.saddle, 'R', Items.redstone, 'I', Items.iron_ingot, 'W',
                    new ItemStack(bikePart, 1, 0), 'G', Items.gold_ingot);
            GameRegistry.addRecipe(new ItemStack(bikePart, 1, 2), "W", "F", "R", 'W', Items.water_bucket, 'F', Blocks.furnace, 'R', Items.redstone);
            EntityRegistry.registerModEntity(EntityMaroonMarauder.class, "MaroonMarauder", 1, this, 40, 1, true);
            for (int i = 0; i < 2; i++) {
                GameRegistry.addRecipe(new ItemStack(ride, 1, i), "W", "C", "E", 'W', new ItemStack(Blocks.wool, 1, 14 + i), 'C', new ItemStack(bikePart, 1, 1), 'E',
                        new ItemStack(bikePart, 1, 2));
            }
            EntityRegistry.registerModEntity(EntityBlackWidow.class, "BlackWidow", 2, this, 40, 1, true);
        }
        if (enableLawnMower) {
            GameRegistry.addRecipe(new ItemStack(ride, 1, 2), " B ", "III", 'B', Items.boat, 'I', Items.iron_ingot);
            EntityRegistry.registerModEntity(EntityLawnMower.class, "LawnMower", 3, this, 40, 1, true);
            GameRegistry.addShapelessRecipe(new ItemStack(ride, 1, 3), Items.iron_ingot, Items.wheat_seeds);
        }*/
        if(enableTVMod){
            tv = new ItemTV();
            tvRemote = new ItemTVRemote();
            GameRegistry.addShapelessRecipe(new ItemStack(tvRemote), Items.redstone, Items.iron_ingot);
            GameRegistry.addRecipe(new ItemStack(tv), "G", "P", "R", 'G', Blocks.glass, 'P', Items.painting, 'R', Items.redstone);
            EntityRegistry.registerModEntity(EntityTV.class, "TV", 4, this, 50, 3, false);
            width = config.getInt("TvWidth", "TV", 4, 0, Integer.MAX_VALUE, "Size in blocks");
            height = config.getInt("TvHeight", "TV", 2, 0, Integer.MAX_VALUE, "Size in blocks");
            vw = config.getInt("VidWidth", "TV", 320/*640*/, 0, Integer.MAX_VALUE, "Video Resolution");
            vh = config.getInt("VidHeight", "TV", 240/*480*/, 0, Integer.MAX_VALUE, "Video Resolution");
            vfps = config.getInt("VidFps", "TV", 25/*30*/, 0, Integer.MAX_VALUE, "Video Framerate");
            asr = config.getInt("AudSR", "TV", 44100/**/, 0, Integer.MAX_VALUE, "Audio SampleRate");
            ac = config.getInt("AudCh", "TV", 2/**/, 0, Integer.MAX_VALUE, "Audio Channels");
            soundRange = config.getInt("SoundRange", "TV", 40, 0, Integer.MAX_VALUE, "Radius in blocks");
            audPreFeed = config.getInt("AudPreFeed", "TV", 1, 1, Integer.MAX_VALUE, "How many frames should be pre-feed (audio)");
            preWaitDelay = config.getInt("PreWaitDelay", "TV", 4, 1, Integer.MAX_VALUE, "How many frames should be buffered");
            //HD = config.getBoolean("HDEnabled", "TV", true, "");
            shuffle = config.getBoolean("ShuffleEnabled", "TV", false, "Shuffle Mode");
            linscale = config.getBoolean("LinearScale", "TV", false, "Enable Linear Texture Scaling (smoother image)");

            //loadGamePath();
        }
        if(config.hasChanged())
            config.save();
        if(event.getSourceFile().getName().endsWith(".jar")){
            proxy.tryCheckForUpdate();
        }
    }

    public static boolean worldNotDied=false;

    @SubscribeEvent
    public void worldUnload(WorldEvent.Unload event){
        //SELF LEAVING
        System.out.println("Unload World");
        worldNotDied=false;
    }

    @SubscribeEvent
    public void worldLoad(WorldEvent.Load event){
        //SELF LEAVING
        System.out.println("Load World");
        worldNotDied=true;
    }

    public static void loadLibs(URL link) {
        System.out.println("Begin load library "+link.getFile());
        File local=new File(libDir,link.getFile().substring(link.getFile().lastIndexOf("/")+1));
        if(!local.exists()){
            //DOWLOAD IT
            System.out.println("Begin Download: "+link.toString());
            try{
                HttpURLConnection uc=(HttpURLConnection)link.openConnection();
                uc.setDoInput(true);
                String redir=uc.getHeaderField("Location");
                while(redir!=null){
                    System.out.println("Redirect to: "+redir);
                    uc=(HttpURLConnection)new URL(redir).openConnection();
                    uc.setDoInput(true);
                    redir=uc.getHeaderField("Location");
                }
                InputStream is=uc.getInputStream();
                OutputStream os= new FileOutputStream(local);
                byte[] buf=new byte[65536];
                int len;
                while((len=is.read(buf))>-1){
                    os.write(buf,0,len);
                    os.flush();
                }
                os.close();
                is.close();
                System.out.println("Download finished");
                diddl=true;
            }catch(Exception ex){ex.printStackTrace();}
        }


    }

    public static boolean diddl=false;
    public static File gameDir,libDir;

    public static void loadGamePath(){
        if(FMLCommonHandler.instance().getSide()== Side.CLIENT){
            gameDir=Minecraft.getMinecraft().mcDataDir;
        } else {
            gameDir=FMLCommonHandler.instance().getMinecraftServerInstance().getFile("mods").getParentFile();
        }
        libDir=new File(gameDir,"mods");
        libDir.mkdirs();
    }

    public static void registerItem(Item i) {
        String un=i.getUnlocalizedName().substring(5);
        GameRegistry.registerItem(i,un);
    }

    public static void registerRender(Item i) { //TODO: BAUMFIX + RECOPY RESOURCES
        //if(FMLCommonHandler.instance().getSide()==Side.CLIENT){
        System.out.println("Load tex paraknight:"+i.getUnlocalizedName().substring(5));
            Minecraft
                    .getMinecraft()
                    .getRenderItem()
                    .getItemModelMesher()
                    .register(i, 0,
                            new ModelResourceLocation("paraknight:"+i.getUnlocalizedName().substring(5), "inventory"));
        //}
    }
    
    @SideOnly(Side.CLIENT)
	public static void bindTexture(int i) {
		GlStateManager.bindTexture(i);
	}
}
