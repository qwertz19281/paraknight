package tvmod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import org.lwjgl.input.Keyboard;

import core.ModPack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public final class ItemTVRemote extends Item {
	
	public ItemTVRemote() {
		super();
		setUnlocalizedName("tvRemote");
		setMaxStackSize(1);
        setCreativeTab(CreativeTabs.tabDecorations);
        core.ModPack.registerItem(this);
	}
	
	@Override
	public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player) {
        if(world.isRemote) {
            MovingObjectPosition movingObjectPosition = ModPack.proxy.getMouseOver();
            if (movingObjectPosition != null && movingObjectPosition.entityHit instanceof EntityTV) {
                ((EntityTV) movingObjectPosition.entityHit).onRemoteClick(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT));
            }
        }
        world.playSoundAtEntity(player, "random.click", 0.1F, 0.1F);
        return itemstack;
	}
}
