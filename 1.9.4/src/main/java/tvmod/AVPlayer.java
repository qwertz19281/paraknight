package tvmod;

import core.ModPack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.world.World;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;

import org.lwjgl.opengl.GL11;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ProcessBuilder.Redirect;
import java.nio.ByteBuffer;
import java.util.Random;

public class AVPlayer implements Runnable {
	public EntityTV tv;
	public World wo;

	public AVPlayer(EntityTV etv, World w) {
		tv = etv;
		wo = w;
	}

	public void assertPlayerHasTV() {
		if (tv == null) {
			throw new AssertionError("The AVPlayer has no TV");
		} else if (tv.avPlayer == null) {
			throw new AssertionError("The TV of the AVPlayer has no AVPlayer");
		}
		if (tv.avPlayer != this) {
			throw new AssertionError("The TV of the AVPlayer doesn't have the correct AVPlayer");
		}
		if (tv.avPlayer.tv != tv) {
			throw new AssertionError("The AVPlayer of the TV of the AVPlayer doesn't have the TV of the AVPlayer"); //joking
		}
	}

	private ByteBuffer imgBuffer = null;
	public boolean newtex = true; //Image has updated

	public static BufferedImage noSignalImage;
	private static int noSigTexID = -1;

	private int texID = -1, currID = -1, texW = -1, texH = -1, texWH = -1;
	private long frameLen=-1;//frame time in millis
	public boolean nonosig=true;

	//@SideOnly(Side.CLIENT)
	public void initScreenBuffer() {
		if(!wo.isRemote) {return;}
		if (texID == -1) {
			texID = GL11.glGenTextures();
		}
		if (noSignalImage == null) {
			InputStream inputstream = this.getClass().getResourceAsStream("/assets/paraknight/textures/nosignal.png");
			try {
				BufferedImage noSignalImg = ImageIO.read(inputstream);
				noSigTexID = GL11.glGenTextures();
				TextureUtil.uploadTextureImage(noSigTexID, noSignalImg);
				//imgBuffer = GLAllocation.createDirectByteBuffer(noSignalImage.getWidth() * noSignalImage.getHeight() * 3);
				inputstream.close();
				//currID = noSigTexID;
				noSignalImage = noSignalImg;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(nonosig){
			assertPlayerHasTV();
			currID = noSigTexID;
			nonosig=false;
		}
	}

	public void loadBufferToRender() {
		if (newtex) {
			//ByteBuffer imgb = imgBuffer;
			if (imgBuffer != null) {
				GL11.glBindTexture(GL11.GL_TEXTURE_2D, texID);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, ModPack.linscale?GL11.GL_LINEAR:GL11.GL_NEAREST);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, ModPack.linscale?GL11.GL_LINEAR:GL11.GL_NEAREST);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
				GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
				synchronized(mu) {
					imgBuffer.position(0);//.limit(entityTV.wh);
					GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGB, texW, texH, 0, GL11.GL_RGB, GL11.GL_UNSIGNED_BYTE, imgBuffer);
					imgBuffer.position(0);//.limit(entityTV.wh);
					newtex = false;
				}
			}
		}
		ModPack.bindTexture(currID);
	}

	public String currentVideoPath = null;
	public boolean isVideoPaused = false, shouldSkip = false, isVideoOver = false, isVideoPlaying = false;
	public boolean running = false; //method run() is running
	private double divFrameLen=0;
	private long frameOff=0,samplesPerFrame=-1;
	public Process vp,ap;
	public SourceDataLine soundLine=null;
	private FloatControl floatControl=null;
	protected Object mu=new Object();

	public Thread t=null;

	public void run() {
		//Running Video Player #ORALDURCHBOHRUNG
		running = true;
		try {
			currID = texID;

			texW = ModPack.vw;
			texH = ModPack.vh;
			texWH = texW * texH;
			frameLen=(long)(1000d/((double) ModPack.vfps));
			divFrameLen=((double)frameLen)/1000d;
			samplesPerFrame=(long)(((double) ModPack.asr)*divFrameLen);
			frameOff=0;
			System.out.println("Begin playing " + currentVideoPath);
			isVideoPlaying = true;
			if (!wo.isRemote) {
				return;
			}
			firstTimestampInStream = 0;
			systemClockStartTime = 0;

			byte[] videoBuffer = new byte[texWH * 3/*4*/];

			int afs = ModPack.ac * 2/*4*/;
			byte[] audioBuffer = new byte[(int)samplesPerFrame*afs];
			byte[] cleanAudioBuffer = new byte[(int)samplesPerFrame*afs];
			int prefeedcnt= ModPack.audPreFeed;
			AudioFormat audioFormat = new AudioFormat(samplesPerFrame* ModPack.vfps, 16/*4*/, ModPack.ac, true, false);

			/*String qwpkdef=currentVideoPath;
			if(qwpkdef.contains(" ")){
				File newPath=new File(qwpkdef.replace(" ",""));
				newPath.getParentFile().mkdirs();
				File oldFile=new File(qwpkdef);
				System.out.println("Renaming "+oldFile.getAbsolutePath()+" to "+newPath.getAbsolutePath());
				oldFile.renameTo(newPath);
				currentVideoPath=newPath.getAbsolutePath();
				qwpkdef=currentVideoPath;
			}*/
			if (ModPack.t.isAlive()) {
				System.out.println("INFO: FFmpeg updater still running, please wait");
				if (wo.isRemote) {
					tv.chatFFMsg();
				}
				ModPack.t.join();
			}
			//            String vs = "ffmpeg -loglevel quiet -i " + qwpkdef + " -f rawvideo -r 1000/" + frameLen/*vf vf vf vf*/ + " -vf scale=" + ModPack.vw + ":" + ModPack.vh + " -pix_fmt rgb24 -vcodec rawvideo pipe:1";
			//            System.out.println(vs);
			//            vp = Runtime.getRuntime().exec(vs);
			//            String as = "ffmpeg -loglevel quiet -i " + qwpkdef + " -f s16le -ar " + samplesPerFrame * ModPack.vfps + " -ac " + ModPack.ac + " pipe:1";
			//            System.out.println(as);
			//            ap = Runtime.getRuntime().exec(as);
			vp=launchFF(new String[] {
					"ffmpeg","-v","warning","-i",currentVideoPath,
					"-f","rawvideo","-r","1000/"+frameLen,
					"-s",ModPack.vw+"x"+ModPack.vh,
					"-pix_fmt","rgb24","-c:v","rawvideo","-"
			});
			ap=launchFF(new String[] {
					"ffmpeg","-v","warning","-i",currentVideoPath,
					"-f","s16le","-ar",String.valueOf(samplesPerFrame*ModPack.vfps),
					"-ac",String.valueOf(ModPack.ac),
					"-"
			});
			InputStream vis = vp.getInputStream();
			InputStream ais = ap.getInputStream();

			try {
				soundLine = (SourceDataLine) AudioSystem.getSourceDataLine(audioFormat);
				soundLine.open(audioFormat);
			} catch (LineUnavailableException e) {
				e.printStackTrace();
			}
			for (int i = 0; i < prefeedcnt; i++) {
				soundLine.write(cleanAudioBuffer, 0, cleanAudioBuffer.length);
			}
			if (soundLine != null && soundLine.isControlSupported(FloatControl.Type.MASTER_GAIN)) {
				floatControl = ((FloatControl) soundLine.getControl(FloatControl.Type.MASTER_GAIN));
				floatControl.setValue(-80);
			}
			//boolean notSkipFrames = true, soundOff = true;
			firstTimestampInStream = 0;
			systemClockStartTime = 0;
			soundLine.start();
			while (!shouldSkip && !isVideoOver && ModPack.worldNotDied) {
				if (isVideoPaused||Minecraft.getMinecraft().isGamePaused()) {
					soundLine.write(cleanAudioBuffer, 0, cleanAudioBuffer.length);
				} else {
					int rd = 0;
					while (rd < audioBuffer.length) {
						int lrd = ais.read(audioBuffer, rd, audioBuffer.length - rd);
						if (lrd == -1) {
							isVideoOver = true;
							break;
						}
						rd+=lrd;
					}

					soundLine.write(audioBuffer, 0, audioBuffer.length);
					//if(soundOff){soundLine.start();soundOff=false;}
					if (imgBuffer == null||imgBuffer.capacity() != videoBuffer.length) {
						imgBuffer = GLAllocation.createDirectByteBuffer(texWH * 3);
					}
					/*if (imgBuffer.capacity() != videoBuffer.length) {
						imgBuffer.clear();
						imgBuffer = GLAllocation.createDirectByteBuffer(texWH * 3);
					}*/
					if (prefeedcnt != 0) {
						synchronized(mu) {
							imgBuffer.position(0);
							imgBuffer.clear();
							imgBuffer.position(0);
							newtex = true;
						}
						prefeedcnt--;
					} else {
						rd = 0;
						while (rd < videoBuffer.length) {
							int lrd = vis.read(videoBuffer, rd, Math.min(16384,videoBuffer.length - rd));
							if (rd == -1) {
								isVideoOver = true;
								break;
							}
							rd+=lrd;
						}
						synchronized(mu) {
							imgBuffer.position(0);
							imgBuffer.put(videoBuffer);
							imgBuffer.position(0);
							newtex = true;
						}

					}
				}

				frameOff += frameLen;
				//soundLine.drain();
				/*notSkipFrames = */sleepUntilReady(frameOff); //TODO Implement frame skip
			}

			floatControl=null;

			if (soundLine != null) {
				soundLine.drain();
				soundLine.close();
				soundLine = null;
			}

			isVideoOver = true;
			isVideoPlaying = false;
			currID = noSigTexID;
		} catch (Exception ex) {
			ex.printStackTrace();
			try {
				floatControl=null;
				isVideoOver = true;
				isVideoPlaying = false;
				soundLine.close();
			} catch (Exception ey) {
				ey.printStackTrace();
			}
			currID = noSigTexID;
		}

		running=false;
	}

	public Process launchFF(String[] opts) throws IOException {
		ProcessBuilder pb=new ProcessBuilder(opts);
		pb.redirectError(Redirect.INHERIT);
		return pb.start();
	}

	@SuppressWarnings("deprecation")
	public void killPlayback(){
		if(t!=null){t.stop();}
		if(vp!=null){vp.destroy();}
		if(ap!=null){ap.destroy();}
		currID=noSigTexID;
		if (soundLine != null) {
			soundLine.drain();
			soundLine.close();
			soundLine = null;
		}
		t=null;
		//omg=null;
	}

	private long firstTimestampInStream = 0, systemClockStartTime = 0;

	private boolean sleepUntilReady(long ts) {
		if (firstTimestampInStream == 0) {
			firstTimestampInStream = ts;
			systemClockStartTime = System.currentTimeMillis();
		} else {
			long millisecondsToSleep = (((ts-firstTimestampInStream))-(System.currentTimeMillis() - systemClockStartTime + 50 /*tolerance*/));
			if (millisecondsToSleep > 0) {
				try{Thread.sleep(millisecondsToSleep);}catch(InterruptedException ignored){}
			} else if(millisecondsToSleep<0){
				return false;
			}
		}
		return true;
	}

	public void startPlayer(String vidpth, boolean force){
		if((!running)||force) {
			killPlayback();
			currentVideoPath=vidpth;
			t = new Thread(this);
			t.setDaemon(true);
			t.start();
		}
	}

	//@SideOnly(Side.CLIENT)
	public void calculateVolume() {
		if(!wo.isRemote) {return;}
		if(floatControl!=null) {
			float vol = -80;
			float distToPlayer = tv.dist2player();
			if (distToPlayer > ModPack.soundRange)
				vol = -80;
			if (distToPlayer == 0)
				vol = 6;
			vol = ((1 - (distToPlayer / ModPack.soundRange)) * 86) - 80;
			floatControl.setValue(Math.max(-80f,vol));
		}
	}

	public static byte[] RNDSTRCHARS = "ABCDabcdEFGHefghIJKLijklMNOPmnopQRSTqrstUVWXuvwxYZyz".getBytes();
	public static Random RNDSTRRND=new Random(7564893056730455875l);

	public static String nextRandomString(int strlen) {
		byte[] buf=new byte[strlen];
		for (int idx = 0; idx < buf.length; ++idx) {
			buf[idx] = RNDSTRCHARS[RNDSTRRND.nextInt(RNDSTRCHARS.length)];
		}
		return new String(buf);
	}
}
