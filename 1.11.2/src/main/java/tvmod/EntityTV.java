package tvmod;


import core.ModPack;
import net.minecraft.entity.MoverType;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EntityTV extends Entity {
	private static ArrayList<String> videoPathes=new ArrayList<String>();
	private int tickCounter;
	private int direction, xPos, yPos, zPos;
	private String currentVideoPath;

	public EntityTV(World world) {
		super(world);
		setSize(ModPack.width, ModPack.height);
	}

	public EntityTV(World world, int i, int j, int k, int l) {
		this(world);
		xPos = i;
		yPos = j;
		zPos = k;
		setPosAndAABB(l);
	}

	@Override
	protected void entityInit() {
	}
	
	@SideOnly(Side.CLIENT)
	protected void chatFFMsg() {
		Minecraft.getMinecraft().player.sendMessage(new TextComponentString("FFmpeg updater still running, please wait"));
	}

	//@Override
	public void setPosAndAABB(int orientation) {
		direction = orientation;
		double xSize = getXSize();
		double zSize = getZSize();
		double xPosBlock = (double) xPos + 0.5F;
		double yPosBlock = (double) yPos + 0.5F;
		double zPosBlock = (double) zPos + 0.5F;
		//double offsetOutOfBlock = 0.5625F;
		double offsetOutOfBlock = 1.0D;
		switch (orientation) {
			case 0:
				zPosBlock -= offsetOutOfBlock;
				xPosBlock -= (xSize%2==0)?0.5F:0;
				//xPosBlock=(double)((int)(xPosBlock));
				break;
			case 1:
				xPosBlock -= offsetOutOfBlock;
				zPosBlock += (zSize%2==0)?0.5F:0;
				//zPosBlock=(double)((int)(zPosBlock+0.9D));
				break;
			case 2:
				zPosBlock += offsetOutOfBlock;
				xPosBlock += (xSize%2==0)?0.5F:0;
				//xPosBlock=(double)((int)(xPosBlock+0.9D));
				break;
			case 3:
				xPosBlock += offsetOutOfBlock;
				zPosBlock -= (zSize%2==0)?0.5F:0;
				//zPosBlock=(double)((int)(zPosBlock));
				break;
		}
		yPosBlock += (ModPack.height%2==0)?0.5F:0;
		//YPSILON=yPosBlock; //TODO: YFIX
		setPositionAndRotation(xPosBlock, yPosBlock, zPosBlock, orientation * 90, 0);
		setPositionAndRotation(xPosBlock, yPosBlock, zPosBlock, orientation * 90, 0);
		setPosition(xPosBlock,yPosBlock,zPosBlock);
		move(MoverType.SELF,0,0,0);
	}

	private double getXSize(){
		if(direction == 0 || direction == 2){
			return ModPack.width;
		}
		return 0.0625D;
	}

	private double getZSize(){
		if(direction == 0 || direction == 2){
			return 0.0625D;
		}
		return ModPack.width;
	}

	@Override
	public void setPosition(double x, double y, double z){
		super.setPosition(x, y, z);//TODO: YFIX
		//y=YPSILON;//TODO: YFIX
		double offset = -0.00625D;
		baumbox=new AxisAlignedBB(x - 0.8D/2 - offset, y - height/2 - offset, z - 0.8D/2 - offset, x + 0.8D/2 + offset, y + height/2 + offset, z + 0.8D/2 + offset);
		setEntityBoundingBox(baumbox);//TODO: YFIX
		//setEntityBoundingBox(new AxisAlignedBB(x - ModPack.width/2 - offset, y - height/2 - offset, z - 0.0625F/2 - offset, x + ModPack.width/2 + offset, y + height/2 + offset, z + 0.0625F/2 + offset));//TODO: YFIX
		//Die Box ist in den Richtungen falsch, in denen man von West/East draufschauf
	}

	private AxisAlignedBB baumbox;

	@Override
	public void onUpdate() {
		if (world.isRemote) {
			if (videoPathes == null) {
				videoPathes = new ArrayList<String>();
				loadVideoPathes();
			}
			if (currentVideoPath == null)
				currentVideoPath = ModPack.shuffle ? getRandomVideoPath() : getNextVideoPath();
			if (avPlayer.nonosig) {
				avPlayer.initScreenBuffer();
			}
			avPlayer.calculateVolume();
			if (avPlayer.isVideoOver) {
				avPlayer.shouldSkip = false;
				avPlayer.isVideoOver = false;
				if (feedPaused()) {
					avPlayer.isVideoPaused = false;
					return;
				}
				currentVideoPath = ModPack.shuffle ? getRandomVideoPath() : getNextVideoPath();
				avPlayer.killPlayback();
				avPlayer.startPlayer(currentVideoPath,true);
			}
		} else if (tickCounter++ >= 100) {
			tickCounter = 0;
			if (!canStay()) {
				setDead();
			}
		}
	}

	@Override
	public void setDead(){
		super.setDead();
		avPlayer.killPlayback();
		if(!world.isRemote)
			world.spawnEntity(new EntityItem(world, posX, posY, posZ, new ItemStack(ModPack.tv)));
	}

	@SideOnly(Side.CLIENT)
	public void loadVideoPathes() {
		videoPathes.clear();
		File dir = new File(FMLClientHandler.instance().getClient().mcDataDir,"TV");
		File[] files=null;
		if (dir.exists() || dir.mkdirs()) {
			files = dir.listFiles();
		}
		if(files!=null) {
			Arrays.sort(files);
			for (File file : files) {
				if(!file.getName().endsWith(".off")) {
					videoPathes.add(file.getAbsolutePath());
				}
			}
		}
	}


	//@Override
	public boolean canStay() {
		if(true){return true;} //TODO: YFIX
		if (world.getCollisionBoxes(this, getEntityBoundingBox()).size() > 0)
			return false;
		int xBlockSize = ModPack.width;
		int yBlockSize = ModPack.height;
		int xBlockPos = xPos;
		int yBlockPos = yPos;
		int zBlockPos = zPos;
		if (direction == 0)
			xBlockPos = MathHelper.floor(posX - (double) ((float) xBlockSize/2));
		else if (direction == 1)
			zBlockPos = MathHelper.floor(posZ - (double) ((float) xBlockSize/2));
		else if (direction == 2)
			xBlockPos = MathHelper.floor(posX - (double) ((float) xBlockSize/2));
		else if (direction == 3)
			zBlockPos = MathHelper.floor(posZ - (double) ((float) xBlockSize/2));
		yBlockPos = MathHelper.floor(posY - (double) ((float) yBlockSize/2));
		for (int i = 0; i < xBlockSize; i++)
			for (int j = 0; j < yBlockSize; j++) {
				Material material;
				if (direction == 0 || direction == 2)
					material = world.getBlockState(new BlockPos(xBlockPos + i, yBlockPos + j, zPos)).getMaterial();
				else
					material = world.getBlockState(new BlockPos(xPos, yBlockPos + j, zBlockPos + i)).getMaterial();
				if (!material.isSolid())
					return false;
			}
		List<?> list = world.getEntitiesWithinAABBExcludingEntity(this, getEntityBoundingBox());
		for (Object object : list)
			if (object instanceof EntityTV)
				return false;
		return true;
	}

	@Override
	public boolean canBeCollidedWith() {
		return true;
	}

	@Override
	public AxisAlignedBB getCollisionBox(Entity entity)
	{
		return baumbox;
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBox(){return baumbox;}

	@Override
	public boolean isPushedByWater()
	{
		return false;
	}

	@Override
	public boolean attackEntityFrom(DamageSource src, float i) {
		if (i>0.001F && !isDead && !world.isRemote) {
			setBeenAttacked();
			setDead();
		}
		return true;
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbttagcompound) {
		nbttagcompound.setInteger("TileX", xPos);
		nbttagcompound.setInteger("TileY", yPos);
		nbttagcompound.setInteger("TileZ", zPos);
		nbttagcompound.setByte("Dir", (byte) direction);
		if(currentVideoPath!=null)
			nbttagcompound.setString("VidPath", currentVideoPath);
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbttagcompound) {
		xPos = nbttagcompound.getInteger("TileX");
		yPos = nbttagcompound.getInteger("TileY");
		zPos = nbttagcompound.getInteger("TileZ");
		direction = nbttagcompound.getByte("Dir");
		currentVideoPath = nbttagcompound.getString("VidPath");
		setPosAndAABB(direction);
	}

	@Override
	public void move(MoverType type, double d, double d1, double d2) {
		if (d * d + d1 * d1 + d2 * d2 > 0.001D) {
			setDead();
		}
	}

	@Override
	public void addVelocity(double d, double d1, double d2) {
		if (d * d + d1 * d1 + d2 * d2 > 0.001D) {
			setDead();
		}
	}

	@Override
	public boolean isCreatureType(EnumCreatureType type, boolean forSpawnCount){
		return false;
	}

	@SideOnly(Side.CLIENT)
	private String getRandomVideoPath() {
		loadVideoPathes();
		if(videoPathes.size()<1)
			return null;
		return videoPathes.get(this.rand.nextInt(videoPathes.size()));
	}

	@SideOnly(Side.CLIENT)
	private String getNextVideoPath() {
		loadVideoPathes();
		if(videoPathes.size()<1)
			return null;
		boolean pathFound = false;
		for (String path : videoPathes) {
			if(pathFound)
				return path;
			if(path.equals(currentVideoPath))
				pathFound = true;
		}
		return videoPathes.get(0);
	}

	public void onRemoteClick(boolean playerSneaking) {
		if (!avPlayer.isVideoPlaying) {
			avPlayer.killPlayback();
			avPlayer.startPlayer(currentVideoPath,true);
		} else if (playerSneaking) {
			avPlayer.killPlayback();
			currentVideoPath = ModPack.shuffle?getRandomVideoPath():getNextVideoPath();
			avPlayer.startPlayer(currentVideoPath,true);
		} else {
			reversePause();
		}
	}

	private synchronized void reversePause(){
		System.out.println(avPlayer.isVideoPaused?"play":"pause");
		avPlayer.isVideoPaused = !avPlayer.isVideoPaused;
	}

	public synchronized boolean feedPaused(){
		return avPlayer.isVideoPaused;
	}

	public AVPlayer avPlayer=new AVPlayer(this,world);

	@SideOnly(Side.CLIENT)
    protected float dist2player() {
		return getDistanceToEntity(Minecraft.getMinecraft().player);
	}
}
