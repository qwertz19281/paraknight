package tvmod;

import net.minecraft.client.Minecraft;
//import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;

import core.ModPack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
//import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public final class ItemTVRemote extends Item {
	
	public ItemTVRemote() {
		super();
		//setTextureName("tvmod:tvremote");
		setUnlocalizedName("tvremote");
		setMaxStackSize(1);
        setCreativeTab(CreativeTabs.DECORATIONS);
        GameRegistry.register(this, new ResourceLocation("paraknight","tvremote"));
	}

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
        if(worldIn.isRemote) {
            //System.out.println("Pos "+player.getPositionVector().toString());
            //System.out.println("Look "+player.getLookVec().toString());
            //FMLClientHandler.instance();
            RayTraceResult movingObjectPosition=Minecraft.getMinecraft().objectMouseOver;
            //MovingObjectPosition movingObjectPosition = ModPack.proxy.getMouseOver();
            //System.out.println("MOP "+((movingObjectPosition==null)?"null":movingObjectPosition.toString()));
            if (movingObjectPosition != null && movingObjectPosition.entityHit instanceof EntityTV) {
//                System.out.println("Hit TV");
                ((EntityTV) movingObjectPosition.entityHit).onRemoteClick(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT));
            } else {
//                System.out.println("Hit not");
                return new ActionResult(EnumActionResult.PASS, playerIn.getHeldItem(handIn));
            }
        }
        //world.playSoundAtEntity(player, "random.click", 0.1F, 0.1F);
        //world.playSound(player,player.posX,player.posY,player.posZ, ModPack.click,SoundCategory.BLOCKS,1,1);
        return new ActionResult(EnumActionResult.SUCCESS, playerIn.getHeldItem(handIn));
	}
}
