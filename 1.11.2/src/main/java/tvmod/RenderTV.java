package tvmod;

import core.ModPack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

public class RenderTV extends Render<EntityTV> {

	public RenderTV(RenderManager renderManager) {
		super(renderManager);
	}

	/*	private void renderTV(int width, int height) {
            float f = (float) (-width) / 2.0F;
            float f1 = (float) (-height) / 2.0F;
            float f2 = -0.5F;
            float f3 = 0.5F;
            int i1=0,j1=0;
            //for (int i1 = 0; i1 < width / 16; i1++)
                //for (int j1 = 0; j1 < height / 16; j1++) {
                    float f4 = f + width;
                    float f5 = f;
                    float f6 = f1 + height;
                    float f7 = f1;
                    GL11.glColor3f(1, 1, 1);
                    float f8 = 0f;
                    float f9 = 1f;
                    float f10 = 0f;
                    float f11 = 1f;
                    float f12 = 0.75F;
                    float f13 = 0.8125F;
                    float f14 = 0.0F;
                    float f15 = 0.0625F;
                    float f16 = 0.75F;
                    float f17 = 0.8125F;
                    float f18 = 0.001953125F;
                    float f19 = 0.001953125F;
                    float f20 = 0.7519531F;
                    float f21 = 0.7519531F;
                    float f22 = 0.0F;
                    float f23 = 0.0625F;
                    Tessellator tessellator = Tessellator.instance;
                    tessellator.startDrawingQuads();
                    tessellator.setNormal(0.0F, 0.0F, -1F);
                    tessellator.addVertexWithUV(f4, f7, f2, f9, f10);
                    tessellator.addVertexWithUV(f5, f7, f2, f8, f10);
                    tessellator.addVertexWithUV(f5, f6, f2, f8, f11);
                    tessellator.addVertexWithUV(f4, f6, f2, f9, f11);
                    tessellator.setNormal(0.0F, 0.0F, 1.0F);
                    tessellator.addVertexWithUV(f4, f6, f3, f12, f14);
                    tessellator.addVertexWithUV(f5, f6, f3, f13, f14);
                    tessellator.addVertexWithUV(f5, f7, f3, f13, f15);
                    tessellator.addVertexWithUV(f4, f7, f3, f12, f15);
                    tessellator.setNormal(0.0F, -1F, 0.0F);
                    tessellator.addVertexWithUV(f4, f6, f2, f16, f18);
                    tessellator.addVertexWithUV(f5, f6, f2, f17, f18);
                    tessellator.addVertexWithUV(f5, f6, f3, f17, f19);
                    tessellator.addVertexWithUV(f4, f6, f3, f16, f19);
                    tessellator.setNormal(0.0F, 1.0F, 0.0F);
                    tessellator.addVertexWithUV(f4, f7, f3, f16, f18);
                    tessellator.addVertexWithUV(f5, f7, f3, f17, f18);
                    tessellator.addVertexWithUV(f5, f7, f2, f17, f19);
                    tessellator.addVertexWithUV(f4, f7, f2, f16, f19);
                    tessellator.setNormal(-1F, 0.0F, 0.0F);
                    tessellator.addVertexWithUV(f4, f6, f3, f21, f22);
                    tessellator.addVertexWithUV(f4, f7, f3, f21, f23);
                    tessellator.addVertexWithUV(f4, f7, f2, f20, f23);
                    tessellator.addVertexWithUV(f4, f6, f2, f20, f22);
                    tessellator.setNormal(1.0F, 0.0F, 0.0F);
                    tessellator.addVertexWithUV(f5, f6, f2, f21, f22);
                    tessellator.addVertexWithUV(f5, f7, f2, f21, f23);
                    tessellator.addVertexWithUV(f5, f7, f3, f20, f23);
                    tessellator.addVertexWithUV(f5, f6, f3, f20, f22);
                    tessellator.draw();
                //}
        }
    */
	public void renderTV2(int ww,int hh){
		float w=((float)ww)/2;
		float h=((float)hh)/2;
		GL11.glBegin(GL11.GL_QUADS);
			GL11.glNormal3f(-w, h, 0);
			GL11.glTexCoord2f(1,1);
			GL11.glVertex3f(-w,-h,0); //Unten Links
			GL11.glTexCoord2f(1,0);
			GL11.glVertex3f(-w,h,0); //Oben Links
			GL11.glTexCoord2f(0,0);
			GL11.glVertex3f(w,h,0); //Oben Rechts
			GL11.glTexCoord2f(0,1);
			GL11.glVertex3f(w,-h,0); //Unten Rechts
		GL11.glEnd();
	}

	//@Override
	//public void doRender(Entity entityTV, double d, double d1, double d2, float f, float f1) {doRender((EntityTV)entityTV,d,d1,d2,f,f1);}
	@Override
	public void doRender(EntityTV entityTV, double d, double d1, double d2, float f, float f1) {
		GL11.glPushMatrix();
		GL11.glTranslatef((float) d, (float) d1, (float) d2);
		GL11.glRotatef(f, 0.0F, 1.0F, 0.0F);
		GL11.glTranslatef(0f, 0f, (float) 0.4f); //TODO: BAUMFIX
		//GL11.glEnable(32826 /* GL_RESCALE_NORMAL_EXT */);
		entityTV.avPlayer.loadBufferToRender();
		renderTV2(ModPack.width, ModPack.height);
		//GL11.glDisable(32826 /* GL_RESCALE_NORMAL_EXT */);
		GL11.glPopMatrix();
	}

	ResourceLocation nosig=new ResourceLocation("paraknight","textures/nosignal.png");

	@Override
	protected ResourceLocation getEntityTexture(EntityTV entity) {
		return null;
	}

	//@Override
	/*protected ResourceLocation getEntityTexture(Entity entityTV) {
		return null;
	}*/
}
