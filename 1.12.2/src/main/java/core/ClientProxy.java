package core;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import tvmod.EntityTV;
import tvmod.RenderTV;

        public class ClientProxy extends CommonProxy {
            @Override
            public void registerHandlers() {
   //             RenderingRegistry.registerEntityRenderingHandler(EntityMaroonMarauder.class, new RenderSteamBike());
   //             RenderingRegistry.registerEntityRenderingHandler(EntityBlackWidow.class, new RenderSteamBike());
   //             RenderingRegistry.registerEntityRenderingHandler(EntityLawnMower.class, new RenderLawnMower());
        RenderingRegistry.registerEntityRenderingHandler(EntityTV.class, new RenderTV(Minecraft.getMinecraft().getRenderManager()));
	//	MinecraftForge.EVENT_BUS.register(new HUDLawnMower(FMLClientHandler.instance().getClient()));

	}

    //@Override
    //public MovingObjectPosition getMouseOver(){
    //    return FMLClientHandler.instance().getClient().objectMouseOver;
    //}

    @Override
    public void tryCheckForUpdate(){
        try {
            Class.forName("mods.mud.ModUpdateDetector").getDeclaredMethod("registerMod", ModContainer.class, String.class, String.class).invoke(null,
                    FMLCommonHandler.instance().findContainerFor(ModPack.instance),
                    "https://raw.github.com/GotoLink/paraknight/master/update.xml",
                    "https://raw.github.com/GotoLink/paraknight/master/changelog.md"
            );
        } catch (Throwable e) {
        }
    }

    @SideOnly(Side.CLIENT)
    public void registerItemRenderer() {
        reg(ModPack.tv);
        reg(ModPack.tvRemote);
    }

    public void reg(Item item) {//try {
            Minecraft.
                    getMinecraft().
                    getRenderItem().
                    getItemModelMesher()
                .register(item, 0, new ModelResourceLocation("paraknight:" + item.getUnlocalizedName().substring(5), "inventory"));
    }//catch(NullPointerException ex){ex.printStackTrace();}}
}
