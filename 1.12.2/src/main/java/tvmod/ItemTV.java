package tvmod;

import net.minecraft.client.Minecraft;
//import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;

public final class ItemTV extends Item {

	public ItemTV() {
		super();
        //setTextureName("tvmod:tv");
		setUnlocalizedName("tv");
		setRegistryName("tv");
		setMaxStackSize(64);
		setCreativeTab(CreativeTabs.DECORATIONS);
        //GameRegistry.register(this, new ResourceLocation("paraknight","tv"));
		ForgeRegistries.ITEMS.register(this);
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos p, EnumHand eh, EnumFacing bf, float par8, float par9, float par10) {
		ItemStack itemstack=player.getHeldItem(eh);
		int blockSide=bf.ordinal();
		if (blockSide == 0 || blockSide == 1)
			return EnumActionResult.PASS;
		byte orientation = 0;
		if (blockSide == 4)
			orientation = 1;
		if (blockSide == 3)
			orientation = 2;
		if (blockSide == 5)
			orientation = 3;
		EntityTV entityTV = new EntityTV(world, p.getX(),p.getY(),p.getZ() , orientation);
		entityTV.loadVideoPathes();
		if (entityTV.canStay()) {
			if (!world.isRemote)
				world.spawnEntity(entityTV);
			itemstack.shrink(1);
		}
		return EnumActionResult.SUCCESS;
	}
}
